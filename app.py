import decimal
import json
import datetime

from flask import Flask
from flask import jsonify
from flask import request
from flask import abort
from flask_cors import CORS
from flask import Response
import psycopg2
import psycopg2.extras
import requests


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
conn = psycopg2.connect('dbname=trainbooking user=postgres password=ilname1999')

def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    if isinstance(obj, datetime.datetime):
        return obj.__str__()
    raise TypeError

def timeconverter(o):
    if isinstance(o, datetime.time):
        return o.__str__()

def sql_command(sql, params=None):
    '''
    Common function to execute sql command

    :param str sql: SQL command
    :param tuple params: tuple(list) of passing parametrs 
    '''
    with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        try:
            if params is None:
                cur.execute(sql)
            else:
                cur.execute(sql, params)
            
            data = None
            if cur.description is not None:
                data = cur.fetchall()
            
            conn.commit()
            cur.close()
            return data
        except:
            conn.rollback()
            return None


@app.route('/stations')
def stations():
    sql = '''
    select * from station order by id_station;
    '''
    data = sql_command(sql)

    return jsonify(data)


@app.route('/stations/<string:station>', methods=['GET'])
def station_info(station):
    sql = '''
    select * from station
    where id_station=(%s);
    '''
    data = sql_command(sql, (station,))

    if len(data) > 0:
        data = data[0]

    return jsonify(data)


@app.route('/stations', methods=['POST'])
def insert_station():
    '''
    Insert station into the table
    '''
    sql = """INSERT INTO station(station_name)
             VALUES(%s) RETURNING id_station, station_name;"""
    try:
        data = sql_command(sql, (request.json['station_name'],))
        return jsonify({'status':'ok', 'added':[{'id':data[0][0], 'station_name':data[0][1]}]})
    except Exception:
        conn.rollback()
        return abort(500)    

@app.route('/stations/<int:station_id>', methods=['PUT'])
def update_station(station_id):
    '''
    Update data about selected station

    :param int station_id: id of station that you want to update
    '''
    data = request.json
    sql = '''UPDATE station
    SET station_name=(%s)
    WHERE id_station=(%s)'''

    try:
        sql_command(sql, (data['station_name'] ,station_id))
        return jsonify({'status':'ok','updated' : [{'id_station': station_id ,'station_name':data['station_name']}]})
    except:
        return abort(500)
    

@app.route('/stations/<int:station_id>', methods=['DELETE'])
def delete_station(station_id):
    '''
    Delete station from the table
    :param int station_id: id of station that you want to delete
    '''
    sql  ='''DELETE FROM station WHERE id_station=(%s)'''

    try:
        sql_command(sql, (station_id,))
    except:
        return abort(500)
   
    return jsonify({'status':'ok'})


@app.route('/wagontypes', methods=['GET'])
def wagontypes():
    sql = '''
    select * from wagontype order by id_type;
    '''

    data = sql_command(sql)

    return jsonify(data)
 


@app.route('/wagontypes/<int:id_type>', methods=['GET'])
def get_wagon_type_name(id_type):
    sql = '''
    select * from wagontype where id_type=(%s);
    '''

    data = sql_command(sql, (id_type, ))

    if len(data) > 0:
        data = data[0]

    return jsonify(data)


@app.route('/traintypes', methods=['GET'])
def traintypes():
    sql = '''
    select * from traintype;
    '''
    data = sql_command(sql)
        
    return jsonify(data)


@app.route(r'/trains^', methods=['GET'])
def trains():
    sql = '''
    select train.id_train, train.train_num, t.type_name from train
    join traintype t on train.id_type = t.id_type;
    '''
    data = sql_command(sql)
        
    return jsonify(data)


@app.route(r'/trains/<int:id_train>^', methods=['GET'])
def train_info(id_train):
    sql = '''
    select train.id_train, train.train_num, t.type_name from train
    join traintype t on train.id_type = t.id_type
    where id_train=(%s);
    '''
    data = sql_command(sql, (id_train, ))

    if len(data) > 0:
        data = data[0]
        
    return jsonify(data)


@app.route('/trains', methods=['POST'])
def get_trains():
    from_city = request.json['from']
    to_city = request.json['to']
    date = request.json['date']

    sql = '''select t.id_train, t4.id_station start_station, from_tbl.id_station from_station, s2.id_station to_station, t5.id_station finish_station ,from_tbl.departure_time, arrival_time, t.id_trip, 
     (arrival_date || ' ' || arrival_time)::timestamp - (from_tbl.departure_date || ' ' || from_tbl.departure_time)::timestamp duration  from route
    join (select route.id_trip, s.id_station, route.departure_time, route.departure_date from route
    join trip on route.id_trip = trip.id_trip
    join station s on route.id_station = s.id_station
    where route.departure_date=(%s) and s.station_name=(%s)) from_tbl on route.id_trip=from_tbl.id_trip
    join station s2 on route.id_station = s2.id_station
    join trip t on route.id_trip = t.id_trip
    join train t2 on t.id_train = t2.id_train
    join (select route.id_trip, route.id_station from route
    join station s on route.id_station = s.id_station
    join trip t3 on route.id_trip = t3.id_trip
    where arrival_time is NULL) t4 on t4.id_trip=t.id_trip
    join (select route.id_trip, route.id_station from route
    join station s on route.id_station = s.id_station
    join trip t3 on route.id_trip = t3.id_trip
    where departure_date is NULL) t5 on t5.id_trip=t.id_trip
    where s2.station_name=(%s);'''

    data = sql_command(sql, (date, from_city, to_city))
    
    result = []
    for train in data:
        d = dict()
        r = train_info(train['id_train'])

        d['train'] = r.get_json()
        r = station_info(train['from_station'])
        d['from_station'] = r.get_json()
        r = station_info(train['to_station'])
        d['to_station'] = r.get_json()
        
        r = station_info(train['start_station'])
        d['start_station'] = r.get_json()
        r = station_info(train['finish_station'])
        d['finish_station'] = r.get_json()

        d['departure_time'] = train['departure_time']
        d['arrival_time'] = train['arrival_time']
        d['duration'] = str(train['duration'])
        d['id_trip'] = train['id_trip']
        result.append(d)


    return Response(json.dumps(result, default=timeconverter, ensure_ascii=False), status=200, mimetype='application/json')


@app.route('/wagons/<int:id_wagon>/<int:id_trip>', methods=['GET'])
def get_wagon(id_wagon, id_trip):

    sql = '''
    select * from wagon where id_wagon = (%s) and id_trip=(%s);
    '''

    data = sql_command(sql, (id_wagon, id_trip))

    if len(data) > 0:
        data = data[0]

    return jsonify(data)

@app.route('/places/<int:id_wagon>/<string:from_station>/<string:to_station>')
def get_free_places_wagon(id_wagon, from_station, to_station):
    sql = '''select id_trip from wagon where id_wagon=(%s)'''
    data = sql_command(sql, (id_wagon,))

    if len(data) > 0:
        data = data[0]

    data = get_free_places(data['id_trip'], from_station, to_station).json
    result = list(filter(lambda x:x['id_wagon']==id_wagon, data))
    return jsonify(result[0])


@app.route('/trains/wagons/<int:id_trip>/<string:from_station>/<string:to_station>', methods=['GET'])
def get_free_places(id_trip, from_station, to_station):
    sql = '''select p.id_wagon, p.id_place, min(departure_date || ' ' || departure_time)::timestamp departure_datetime,
       max(arrival_date || ' ' || arrival_time)::timestamp arrival_datetime from ticket
        join place p on ticket.id_place = p.id_place
        join wagon w on p.id_wagon = w.id_wagon
        join trip t on w.id_trip = t.id_trip
        join route r on t.id_trip = r.id_trip
        where (r.id_station = ticket.from_station or r.id_station = ticket.to_station) and (r.id_trip=(%s))
        group by id_passenger, p.id_place;'''
    
    trip_tickets = sql_command(sql, (id_trip,))

    sql = '''select min(departure_date || ' ' || departure_time)::timestamp departure_datetime,
            max(arrival_date || ' ' || arrival_time)::timestamp arrival_datetime from station
            join route r on station.id_station = r.id_station
            where id_trip=(%s) and (station_name=(%s) or station_name=(%s));'''

    shedule = sql_command(sql, (id_trip, from_station, to_station))[0]

    places = dict()#count in taken places in each wagon
    for period in trip_tickets:
        if (period['departure_datetime'] >= shedule['departure_datetime'] and period['departure_datetime'] <= shedule['arrival_datetime']) \
        or (period['arrival_datetime'] >= shedule['departure_datetime'] and period['arrival_datetime'] <= shedule['arrival_datetime']):
            if period['id_wagon'] not in places:
                places[period['id_wagon']] = dict()
                places[period['id_wagon']]['number'] = 1
                places[period['id_wagon']]['places'] = [period['id_place']]
            else:
                places[period['id_wagon']]['number'] += 1
                places[period['id_wagon']]['places'].append(period['id_place'])

    sql = '''select wagon.id_wagon, count(place_num) places from wagon
            join trip t on wagon.id_trip = t.id_trip
            join place p on wagon.id_wagon = p.id_wagon
            where t.id_trip=(%s)
            group by wagon_num, wagon.id_wagon
            order by wagon_num;'''

    data = sql_command(sql, (id_trip, ))
    result = []
    for wagon in data:
        d = dict()
        d = get_wagon(wagon['id_wagon'], id_trip).json
        d['type'] = get_wagon_type_name(d['id_type']).json
        if wagon['id_wagon'] in places:
            d['places_number'] = wagon['places'] - places[wagon['id_wagon']].get('number', 0)
            taken_places = places[wagon['id_wagon']]['places']
        else:
            d['places_number'] = wagon['places']
            taken_places = []
        sql = '''select * from place
                    where id_wagon=(%s);'''
        all_places_in_wagon = sql_command(sql, (wagon['id_wagon'],))
        free_places = list(filter(lambda x:x['id_place'] not in taken_places, all_places_in_wagon))
        d['free_places'] = free_places
        result.append(d)

    return jsonify(result)


def get_price(id_place):
    return jsonify({'price':200})

def get_id_station(station):
    sql = '''select id_station from station
            where station_name=(%s)'''
    
    data = sql_command(sql, (station, ))
    if len(data)>0:
        data = data[0]
    
    return data['id_station']

@app.route('/ticket/buy', methods=['POST'])
def add_ticket():
    from_station = get_id_station(request.json['from_station'])
    to_station = get_id_station(request.json['to_station'])
    id_passenger = request.json['id_passenger']
    id_place = request.json['id_place']
    price = get_price(id_place).json['price']

    #check is this place free
    sql = '''select min(departure_date || ' ' || departure_time)::timestamp departure_datetime,
       max(arrival_date || ' ' || arrival_time)::timestamp arrival_datetime from ticket
    join place p on ticket.id_place = p.id_place
    join wagon w on p.id_wagon = w.id_wagon
    join trip t on w.id_trip = t.id_trip
    join route r on t.id_trip = r.id_trip
    where (r.id_station = ticket.from_station or r.id_station = ticket.to_station) and ticket.id_place=(%s)
    group by id_passenger, p.id_place'''
    place_occupied_time = sql_command(sql, (id_place, ))#periods when place is occupied

    sql = '''select min(departure_date || ' ' || departure_time)::timestamp departure_datetime,
       max(arrival_date || ' ' || arrival_time)::timestamp arrival_datetime from trip
        join route r on trip.id_trip = r.id_trip
        join wagon w on trip.id_trip = w.id_trip
        join place p on w.id_wagon = p.id_wagon
        where p.id_place=(%s) and (id_station=(%s) or id_station=(%s));'''

    shedule = sql_command(sql, (id_place, from_station, to_station))[0] #train shedule
    for period in place_occupied_time:
        if (shedule['departure_datetime'] >= period['departure_datetime'] and shedule['departure_datetime'] <= period['arrival_datetime']) \
        or (shedule['arrival_datetime'] >= period['departure_datetime'] and shedule['arrival_datetime'] <= period['arrival_datetime']):
            return jsonify({'status':'place has been already taken'}), 500

    sql = ''' INSERT INTO ticket(from_station, to_station, price, id_passenger, id_place) VALUES((%s), (%s), (%s), (%s), (%s));'''

    sql_command(sql, (from_station, to_station, price, id_passenger, id_place))

    return jsonify({'status':'ok'})


@app.route('/user/<string:username>', methods=['GET'])
def get_user(username):
    sql = 'select * from passenger where username=(%s)'
    data = sql_command(sql, (username,))

    if len(data) > 0:
        data = data[0]
    else:
        return jsonify({'status':'user not found'}), 500
    
    return jsonify(data)

@app.route('/user/add', methods=['POST'])
def add_user():
    sql = 'INSERT INTO passenger(name, surename, username, password) VALUES((%s), (%s), (%s), (%s))'

    name = request.json['name']
    surename = request.json['surename']
    username = request.json['username']
    password = request.json['password']

    sql_command(sql, (name, surename, username, password))

    return jsonify({'status':'ok'})


@app.route('/ticket/<int:id_ticket>', methods=['DELETE'])
def delete_ticket(id_ticket):
    sql = '''delete from ticket where id_ticket=(%s)'''
    print(id_ticket)
    sql_command(sql, (id_ticket, ))

    return jsonify({'status':'ok'})


@app.route('/user/tickets/<int:id_user>')
def get_user_tickets(id_user):
    sql = '''select id_ticket, train_num, place_num, from_station, to_station, price,
       min(departure_date || ' ' || departure_time)::timestamp departure_datetime,
       max(arrival_date || ' ' || arrival_time)::timestamp arrival_datetime from ticket
        join station s on ticket.from_station = s.id_station
        join place p on ticket.id_place = p.id_place
        join wagon w on p.id_wagon = w.id_wagon
        join trip t on w.id_trip = t.id_trip
        join train t2 on t.id_train = t2.id_train
        join route r on t.id_trip = r.id_trip
        where id_passenger=(%s) and (ticket.from_station=r.id_station or ticket.to_station=r.id_station)
        group by id_ticket, train_num, place_num, from_station, to_station, price;'''

    data = sql_command(sql, (id_user,))

    sql = '''select * from station where id_station=(%s)'''
    for index, ticket in enumerate(data):
        data1 = sql_command(sql, (ticket['from_station'],))
        data[index]['from_station'] = data1[0]

        data1 = sql_command(sql, (ticket['to_station'],))
        data[index]['to_station'] = data1[0]
    return Response(json.dumps(data, default=decimal_default), status=200, mimetype='application/json')

if __name__ == '__main__':
    app.run('127.0.0.1', port=5051)


